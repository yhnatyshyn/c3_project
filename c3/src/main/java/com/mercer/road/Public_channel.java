package com.mercer.road;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.SikuliException;



public class Public_channel extends Channels {
	private static String channel_type=type_public;
//	private String channel_name=public_channel;
	Pattern image =new Pattern();
	// Create pattern for using similiarity (0.9). Default similiarity is 0.7
	
	Pattern channel_name = new Pattern(public_channel).similar(similarity);
	
	public Public_channel(Boolean dialin, Boolean picture)
			throws FindFailed {
		super(channel_type, dialin,picture);
		// TODO Auto-generated constructor stub
	}
	public void join_channel()
			throws SikuliException {
		// TODO Auto-generated method stub
		super.join_channel(channel_name);
	}
	
	public void delete_channel()
			throws SikuliException {
		// TODO Auto-generated method stub
		super.delete_channel(channel_name);
	}
	
	public void exit_channel()  	// This method can be used only when you joined channel
			throws SikuliException {
		// TODO Auto-generated method stub
		super.exit_channel(channel_name);
	}
	
	public void info_channel()
			throws SikuliException {
		// TODO Auto-generated method stub
		super.info_channel(channel_name);
	}
	
	

	
}
