package com.mercer.road;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.Pattern;
import org.sikuli.script.SikuliException;


public class ChannelActions extends JoinChannelActions implements Created_channel, General{
	//Created Channels images:
	//public; public_dialin; public_picture; public_dialin_picture
	
	 Pattern edit_channel=new Pattern();
	 String edit_channel_descritption = "img/Channel_section/edit_channel_description.png";
	 String edit_channel_header = "img/Channel_section/edit_channel_header.png";
	 String dialin_image = "img/Channel_section/dialin.png";
	 
	@Override
	//channel_name depends on channel_type when creating channel names
	public void call_menu_channel(Pattern channel_name) throws SikuliException{
		s.rightClick(channel_name);
		// TODO Auto-generated method stub
	}
	

	@Override
	public void join_channel(Pattern channel_name) throws SikuliException {
	//	call_menu_channel(channel_name);
		s.doubleClick(channel_name);
		// TODO Auto-generated method stub
		
	}	
	public void join_channel_via_menu(Pattern channel_name) throws SikuliException {
		call_menu_channel(channel_name);
		s.click(menuitem_join);
		// TODO Auto-generated method stub
		
	}
	///This method is for joining password protected channels
	public void join_channel(Pattern channel_name, String channel_text_password) throws FindFailed
	{
		s.doubleClick(channel_name);
		s.wait(2.0);
		s.type(channel_text_password);
		s.type(Key.ENTER);
		s.wait(2.0);
	}

	@Override
	public void share_channel(Pattern channel_name,Pattern menuitem_share_option) throws SikuliException {
		call_menu_channel(channel_name);
		s.hover(menuitem_share);
		s.click(menuitem_share_option);
		// TODO Auto-generated method stub
		
	}
	
	public void share_channel_c3(Pattern channel_name) throws SikuliException 
	{   
		call_menu_channel(channel_name);
		s.click(menuitem_share);
		s.click(menuitem_share_c3);
	}
	public void share_channel_email(Pattern channel_name) throws SikuliException 
	{	call_menu_channel(channel_name);
		s.click(menuitem_share);
		s.click(menuitem_share_email);
	}
	public void share_channel_link(Pattern channel_name) throws SikuliException 
	{	
		call_menu_channel(channel_name);
		s.click(menuitem_share);
		s.click(menuitem_share_link);
	}
	@Override
	public void edit_channel_name(Pattern channel,String channel_name) throws SikuliException {
		call_menu_channel(channel);
		s.click(menuitem_edit);
		
		edit_channel.setFilename(edit_channel_header); // creating pattern for edit_channel_header image
		s.click(edit_channel.targetOffset(45, 30).similar(0.8F)); // click on channel name field
		s.type("a",Key.META);
		s.type(Key.DELETE);
		s.type(channel_name);
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete_channel(Pattern channel_name) throws SikuliException {
		call_menu_channel(channel_name);
		s.click(menuitem_delete);
		// TODO Auto-generated method stub
		
	}

	@Override
	public void add_subchannel(Pattern channel_name) throws SikuliException {
		call_menu_channel(channel_name);
		s.click(menuitem_add_subchannel);
		// TODO Auto-generated method stub
		
	}

	@Override
	public void info_channel(Pattern channel_name) throws SikuliException {
		call_menu_channel(channel_name);
		s.click(menuitem_info);
		// TODO Auto-generated method stub
		
	}
	public void close_info_channel() throws SikuliException {
		
		s.click(close_channel_info);
		// TODO Auto-generated method stub

	}


	@Override
	public void edit_channel_fields(Pattern channel, String channel_message,
			String Description, boolean dialin) throws SikuliException {
		
		edit_channel.setFilename(edit_channel_header); // creating pattern for edit_channel_header image
		s.click(edit_channel.targetOffset(45, 60).similar(0.8F)); // click on channel message field
		s.type("a",Key.META);
		s.type(Key.DELETE);
		s.type(channel_message);
		edit_channel.setFilename(edit_channel_descritption);
		s.click(edit_channel.targetOffset(45, 30).similar(0.9F));
		s.type("a",Key.META);
		s.type(Key.DELETE);
		s.type(Description);
		if (dialin==true)
		{
		edit_channel.setFilename(dialin_image);
		s.click(edit_channel.similar(0.9F));}
		//added comments for git
		
	}


	@Override
	public void edit_channel_type(Pattern channel, String channel_message,
			String Description, boolean dialin) throws SikuliException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void edit_channel_picture(Pattern channel, String channel_message,
			String Description, boolean dialin) throws SikuliException {
		// TODO Auto-generated method stub
		
	}
	
	//  mac_set_channel_picture() method is valid for add,edit channel/subchannel dialogue.
	// It click on picture via location dialin checkbox + target offset x
	
	public void mac_set_defaultchannel_picture() throws FindFailed
	{
		s.doubleClick(set_channel_picture);
		s.click(picture_path_Desktop);
		s.click(picture_path_Pictures);
		s.type(Key.RIGHT);
		s.type(Key.RIGHT);
		s.type(Key.ENTER);
	}

}
