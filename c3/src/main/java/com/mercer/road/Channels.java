package com.mercer.road;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.Pattern;
import org.sikuli.script.SikuliException;

public class Channels extends ChannelActions implements General {

	// Pathes
	String System_mac= "Mac/";
	String System_win= "Windows/";		
	String new_path ="src/main/resources/img/";
	String path_Channels = new_path +System_mac+"Channel_section/Channels";
	String path_Channel_section = new_path +System_mac+"Channel_section/";

	Pattern mychannels_panel = new Pattern(
			new_path + System_mac+"Channel_section/my_channels_section.png").similar(0.8F);
	// String channel_type_selection = "img/Channel_section/login.png";
	// password_channel
	String join_password_channel = new_path +System_mac+ "Channel_section/join_password.png";
	String channel_password = new_path +System_mac+ "Channel_section/channel_password_menu.png";
	String delete_channel = new_path + System_mac+ "Channel_section/Channel_actions/menuitem_delete.png";
	
	String channel_name = "Adsnmbfmndsfds";

	public Channels() {
	}

	public void expand_collapse_mychannel_section() throws SikuliException {
		s.click(mychannels_panel);
	}

	public Channels(String channel_type, Boolean dialin, Boolean picture)
			throws FindFailed {
		channel_name = channel_type;
		s.rightClick(mychannels_panel);
		s.wait(2.0);
		// s.click(c3_channels_menu);
		s.click(create_channel_menu2);

		// Fill Channel Fields
		if (dialin == true) {
			s.click(channel_dialin_checkbox);
			channel_name = channel_name + "_dialin";
		}

		if (picture == true) {
			mac_set_defaultchannel_picture();
			channel_name = channel_name + "_picture";
		}

		s.click(channel_name_field);
		s.type(channel_name);
		s.type(Key.TAB);
		s.type(channel_name);

		s.click(channel_description_field);
		s.type(channel_name);

		// change type channel

		s.click(channel_type_menu);

		// channel[0] --public
		// channel[1] --password
		// channel[2] --invite

		switch (channel_type) {
		case "public":
		case "Public":
			channel_type = channel_type_selected[0];
			break;
		case "password":
		case "Password":
			channel_type = channel_type_selected[1];
			break;
		case "invite":
		case "Invite":
			channel_type = channel_type_selected[2];
			break;
		default:
			channel_type = channel_type_selected[1];
			break;

		}
		s.doubleClick(channel_type);
		if (channel_type.equals(channel_type_selected[1])) {
			s.click(channel_pass_field);
			// Create channel
			s.type(channel_text_pas);
		}
		s.click(channel_create_button);
	}

	public void join_channel(String joined_channel_name,
			String channel_text_password, String channel_message)
			throws FindFailed {
		s.doubleClick(joined_channel_name, 200);
		s.wait(2.0);
		s.type(channel_text_password);
		s.type(Key.ENTER);
		s.wait(2.0);
		s.type(channel_message);
		s.wait(2.0);
		s.type(Key.ENTER);
	}

	public void delete_channel(Pattern channel_name) throws FindFailed,
			SikuliException {
		s.rightClick(channel_name);
		s.doubleClick(delete_channel);

	}

	public void edit_channel(String channel_name, String channel_type,
			Boolean dialin) throws FindFailed {

		s.click(c3_channels_menu, 139);
		// s.click(c3_channels_menu);
		s.click(menu_channel_create);

		// Fill Channel Fields
		if (dialin == true) {
			s.click(channel_dialin_checkbox);
			channel_name = channel_name + "_dialin";
		}
		s.click(channel_name_field);
		s.type(channel_name);
		s.type(Key.TAB);
		s.type(channel_name);

		s.click(channel_description_field);
		s.type(channel_name);

		// change type channel

		s.click(channel_type_menu);

		// channel[0] --public
		// channel[1] --password
		// channel[2] --invite

		if (channel_type.equals(channel_type_selected[1])) {
			s.doubleClick(channel_type_selected[1]);
			s.click(channel_password_field);
			// Create channel
			s.type(channel_text_pas);
		}
		s.click(channel_create_button);
	}

}
