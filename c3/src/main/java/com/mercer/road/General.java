package com.mercer.road;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


public interface General {
	//Path Variables
	String System_mac= "Mac/";
	String System_win= "Windows/";
	String new_path ="src/main/resources/img/";
	
	//login variables
	String login = "abc2@cuvox.de";
	String password ="vivox1";
	
	
	// Text channels
	String channel_text_pas ="password"; 	
	
	//Screen variables
	
	
	//Login
	String c3_login = new_path + "img/Login/login.png";
	String c3_login_user = new_path + "img/Login/login_username.png";
//	String channel_password =new_path + "img/channel_password_menu.png";
//	Pattern channel_menu
	//Channel menu items
	String create_channel_menu2 =new_path +System_mac+"Channel_section/Channels/create_channel_menu.png";
	// should be reviewed and deleted
	String c3_channels_menu =new_path + System_mac+"Channel_section/Channels/channel_menu1.png";
	String menu_channel_create =new_path + "Channel_section/Channels/menu_create_channel.png";
	Pattern channel_menu_create = new Pattern(menu_channel_create).similar(0.5f);
	 float similarity =0.9F;
	
		


	// Create channel dialogue
	
	String channel_description_field1 = new_path + System_mac+"Channel_section/Channels/channel_description.png";
	Pattern channel_description_field = new Pattern(channel_description_field1).similar(0.9f);
	String channel_dialin_checkbox = new_path + System_mac+"img/Channel_section/Channels/channel_dialin.png";
	String channel_name_field_str = new_path + System_mac+"Channel_section/Channels/channel_display_name.png";
	Pattern channel_name_field =new Pattern(channel_name_field_str).similar(0.9f);
	
	
	// channel types in dropdown menu
	 String channel_type_menu = new_path + System_mac+"Channel_section/Channels/channel_type.png";
	// password 
	 String channel_type_password = new_path + System_mac+"Channel_section/Channels/channel_type_password.png";
	// public
	//invitation only
	 String channel_password_field =new_path + System_mac+"Channel_section/Channels/password_field.png";
	 String channel_password_field2 =new_path + System_mac+"Channel_section/Channels/channel_password_field_win.png";
	 String channel_create_button = new_path + System_mac+"Channel_section/Channels/create_channel.png";
	 Pattern channel_pass_field = new Pattern(channel_password_field).similar(0.95f);
//	String channel_type_selection = new_path + "img/login.png";
	String join_password_channel= new_path + "img/Channel_section/Password_channel/join_password.png";
	
	
	
	
// Channel types
		String type_public = "Public";
		String type_password = "Password";
		String type_inviteonly = "Invite";
		
		
		
		
		//Generating long string
		String data ="@#$%^&*()_+avxc@#$%^&*()_+avxc!";
		String long_data = "";
	/*	for (int i=0; i<5; i++)
	{
		long_data=long_data+data;
	} */
		

	//Selecting channel types

	String channel_type_selected[] ={new_path + System_mac+"Channel_section/Channels/channel_type_public.png",new_path + System_mac+"Channel_section/Channels/channel_type_password.png",new_path + System_mac+"Channel_section/Channels/channel_type_invite.png"};
	
	
	Screen s =new Screen();
	
	
}
