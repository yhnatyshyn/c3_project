package com.mercer.road;

import org.sikuli.script.Key;
import org.sikuli.script.Pattern;
import org.sikuli.script.SikuliException;


public class JoinChannelActions extends Data implements Joined_channel {
	//Path
	//Path Variables
	String System_mac= "Mac/";
	String System_win= "Windows/";
	String new_path ="src/main/resources/img/";
	String path_Joined_Channel_Actions =new_path+System_mac+"Channel_section/Joined_Channel_Actions/";
			
	Pattern mychannels_panel = new Pattern("img/Channel_section/my_channels_section.png").similar(similarity);
	Pattern selection_channels= new Pattern(path_Joined_Channel_Actions+"channel_text_field.png").targetOffset(-76, 0).similar(0.7f);
	Pattern text_message_field = new Pattern(path_Joined_Channel_Actions+"channel_text_field.png").similar(0.8f);

		// TODO Auto-generated constructor stu

	@Override
	public void call_menu_joined_channel(Pattern channel_name)
			throws SikuliException {
		s.rightClick(channel_name);
		// TODO Auto-generated method stub

	}

	@Override
	public void invite_to_channel(Pattern channel_name) throws SikuliException {
		// TODO Auto-generated method stub

	}

	@Override
	public void find_users_channel(Pattern channel_name,
			Pattern menuitem_share_option) throws SikuliException {
		// TODO Auto-generated method stub

	}

	@Override
	public void add_favorite_channel(Pattern channel_name)
			throws SikuliException {
		// TODO Auto-generated method stub

	}

	@Override
	public void mute_everyone(Pattern channel_name) throws SikuliException {
		// TODO Auto-generated method stub

	}

	@Override
	public void unmute_everyone(Pattern channel_name) throws SikuliException {
		// TODO Auto-generated method stub

	}

	@Override
	public void speak_only(Pattern channel_name) throws SikuliException {
		// TODO Auto-generated method stub

	}

	@Override
	public void exit_channel(Pattern channel_name) throws SikuliException {
		call_menu_joined_channel(channel_name);
		// TODO Auto-generated method stub
		s.doubleClick(exit_channel);
	}
	
	public void Close_all_channels() throws SikuliException {
		// TODO Auto-generated method stub
		s.doubleClick(close_channel,200);
	}

	@Override
	public void view_info(Pattern channel_name) throws SikuliException {
		// TODO Auto-generated method stub

	}

	@Override
	public void mute_unmute_button() throws SikuliException {
		// TODO Auto-generated method stub

	}

	@Override
	public void audio_settings_button() throws SikuliException {
	
		s.doubleClick(audio_settings_channel);
	}

	@Override
	public void close_button() throws SikuliException {
		// TODO Auto-generated method stub

	}

	@Override
	// channel messages
	public void send_long_message() throws SikuliException {
		// TODO Auto-generated method stub
		
		String data ="@#$%^&*()_+avxc@#$%^&*()_+avxc!";
		String long_data = "";
		for (int i=0; i<5; i++)
	{
		long_data=long_data+data;
	}
		String long_message =data;
		s.click(text_message_field);
		s.type("a");
		s.type(Key.ENTER);
		s.type(long_message);
		s.type(Key.ENTER);
	}
	public void send_message(String text) throws SikuliException
	{	s.click(text_message_field);
		s.type(text);
		s.type(Key.ENTER);
	}

	@Override
	public void channel_selection(Pattern channel_selected) throws SikuliException {
		s.click(selection_channels);
		s.click(channel_selected);
	}
	public void channel_selection_public() throws SikuliException {
		s.click(selection_channels);
		s.click(channel_selected_public);
	}
	public void channel_selection_password() throws SikuliException {
		s.click(selection_channels);
		s.click(channel_selected_password);
	}
	public void channel_selection_invite() throws SikuliException {
		s.click(selection_channels);
		s.click(channel_selected_invite);
	}
	public void Expand_collapse_joined_channel() throws SikuliException 
	{
		s.click(joined_channel_panel);
	}
	
}
	

