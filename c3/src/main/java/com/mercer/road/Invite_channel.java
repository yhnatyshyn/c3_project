package com.mercer.road;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.SikuliException;


public class Invite_channel extends Channels{
	private static String channel_type=type_inviteonly;
		
	Pattern channel = new Pattern(invite_channel).similar(similarity);
	
	public Invite_channel(Boolean dialin, Boolean picture)
			throws FindFailed {
		
		super(channel_type, dialin,picture);
		// TODO Auto-generated constructor stub
	}
	// All methods are described in ChannelActions
	public void join_channel()
			throws SikuliException {
		// TODO Auto-generated method stub
		super.join_channel(channel);
	}
	
	public void delete_channel()
			throws SikuliException {
		// TODO Auto-generated method stub
		super.delete_channel(channel);
	}
	public void exit_channel()
			throws SikuliException {
		// TODO Auto-generated method stub
		super.exit_channel(channel);
	}

	public void edit_channel_name() throws SikuliException{
		// TODO Auto-generated method stub
		super.edit_channel_name(channel,channel_name);
		
	}
	public void info_channel()
			throws SikuliException {
		// TODO Auto-generated method stub
		super.info_channel(channel);
	}

}
