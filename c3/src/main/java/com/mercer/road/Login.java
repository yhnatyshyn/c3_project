package com.mercer.road;
import org.sikuli.script.App;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


public class Login implements General_temp {
	
	//Path to Framework images:
	String images_path  = "C:\\Users\\otkachenko\\Desktop\\Eclipse_workspace\\SikuliC3\\img\\Framework\\Login\\";
	String test_images_path  = "C:\\Users\\otkachenko\\Desktop\\Eclipse_workspace\\SikuliC3\\img\\Tests\\Login\\";
	
	//Create Sikuli screen and Pattern
	Screen s =new Screen();
	Pattern image = new Pattern();
	
	//Images
	String login_button = images_path+"login_button.png";
	String login_button_inactive = images_path+"login_button_inactive.png";
	String login_field = images_path+"login_field.png";
	String autologin_enable = images_path+"autologin_enable.png";
	String autologin_disable = images_path+"autologin_disable.png";
	String close_button= images_path+"close_button.png";
	String close_browser= images_path+"close_browser.png";
	String need_an_account= images_path+"need_an_account.png";
	String forgot_password= images_path+"forgot_password.png";
	
	//Test images
	String autologin_check = test_images_path+"autologin_check.png";
	String browser_url_forgot_password = test_images_path+"browser_url_forgot_password.png";
	String browser_url_need_account = test_images_path+"browser_url_need_account.png";
	
	
		//This method will launch C3
		public void launch() {
			App.open(application_path);	
		}
	
		//Login method, accepts user login, password and boolean variable.
		public void login(String login, String password) throws FindFailed
		{
			image.setFilename(login_field);
			s.wait(login_field, 60);
			s.click(image.targetOffset(5, 20));
			s.type("a",Key.CTRL);
			s.type(Key.DELETE);
			s.type(login);
			s.type(Key.TAB);
			s.type(password);
			s.click(login_button);
			s.wait(3.0);
			
		}
		
		public void login_with_autologin(String login, String password) throws FindFailed {
			
			image.setFilename(login_field);
			s.wait(login_field, 60);
			s.click(image.targetOffset(5, 20));
			s.type("a",Key.CTRL);
			s.type(Key.DELETE);
			s.type(login);
			s.type(Key.TAB);
			s.type(password);
			//Click autologin option
			image.setFilename(autologin_enable);
			s.click(image.targetOffset(0, 0).similar(0.95f));
			s.click(login_button);
		
		}
		
		//Will disable autologin through logout and unckeck option
		//and login again.
		public void autologin_disable(String login, String password) throws FindFailed {
			//Logging out from client
			MainScreen window = new MainScreen();
			window.logout();
			//Log in again
			image.setFilename(login_field);
			App.open(application_path);	
			s.wait(login_field, 60);
			s.click(image.targetOffset(5, 20));
			s.type("a",Key.CTRL);
			s.type(Key.DELETE);
			s.type(login);
			s.type(Key.TAB);
			s.type(password);
			//Click autologin option
			image.setFilename(autologin_disable);
			s.click(image.targetOffset(0, 0).similar(0.95f));
			//Login again and exit
			s.click(login_button);
		}
		
		//This method will check if autologin option works
		public void autologin_check() throws FindFailed {
			//Logging out from client
			MainScreen window = new MainScreen();
			window.logout();
			//Login again and exit
			s.click(login_button);
			image.setFilename(autologin_check);
			s.wait(image.similar(0.95f), 30);
			
		}
		
		//This method will close login window
		public void close_window() throws FindFailed {
			image.setFilename(close_button);
			s.wait(image, 30);
			s.click(image.targetOffset(0, 0).similar(0.95f));
				
		}
		
		//This method will check "need an account" link
		public void need_an_account_check() throws FindFailed {
			//Clicking link
			image.setFilename(need_an_account);
			s.wait(image, 30);
			s.click(image.targetOffset(0, 0).similar(0.95f));
			//Waiting for a browser and check a link
			image.setFilename(browser_url_need_account);
			s.wait(image, 30);
			//Closing browser
			image.setFilename(close_browser);
			s.click(image.targetOffset(0, 0).similar(0.95f));
		}
		
		//This method will check "forgot password" link
		public void forgot_password_check() throws FindFailed {
			//Clicking link
			image.setFilename(forgot_password);
			s.wait(image, 30);
			s.click(image.targetOffset(0, 0).similar(0.95f));
			//Waiting for a browser and check a link
			image.setFilename(browser_url_forgot_password);
			s.wait(image, 30);
			//Closing browser
			image.setFilename(close_browser);
			s.click(image.targetOffset(0, 0).similar(0.95f));
		}
		
		//This method will check inactive login button
		//if login or password is incorrect - need to be set up to login and password parameters.
		public void login_button_check(String login, String password) throws FindFailed {
			image.setFilename(login_field);
			s.wait(login_field, 60);
			s.click(image.targetOffset(5, 20));
			s.type("a",Key.CTRL);
			s.type(Key.DELETE);
			s.type(login);
			s.type(Key.TAB);
			s.type(password);
			s.find(login_button_inactive);
			System.out.println("========"+"E-mail: \""+login+"\" or  Password: \""+password+"\" is not valid and login button is inactive");
		}

}
