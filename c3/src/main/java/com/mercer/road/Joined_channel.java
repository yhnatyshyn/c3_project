package com.mercer.road;

import org.sikuli.script.Pattern;
import org.sikuli.script.SikuliException;


public interface Joined_channel extends General {
	String path_Joined_chnannel="img/Channel_section/Joined_Channel_Actions/";
	//Selection Channels where to type when join channel
	//String selection_channels ="img/Channel_section/Joined_Channel_Actions/selection_channel.png";
	String channel_selected_public ="img/Channel_section/Joined_Channel_Actions/channel_selection_public.png";
	String channel_selected_password ="img/Channel_section/Joined_Channel_Actions/channel_selection_password.png";
	String channel_selected_invite ="img/Channel_section/Joined_Channel_Actions/channel_selection_invite.png";
	
	// Expand_collapse_joined_channel
	Pattern joined_channel_panel = new Pattern(path_Joined_chnannel+"joined_channel_icon.png").similar(0.95f);
	
	//Channels
	Pattern public_channel1 =new Pattern(path_Joined_chnannel+"joined_channel_public.png").similar(0.95f);
	Pattern password_channel1 =new Pattern(path_Joined_chnannel+"joined_channel_password.png").similar(0.95f);
	Pattern invite_channel1 =new Pattern(path_Joined_chnannel+"joined_channel_invite.png").similar(0.95f);
	
	//Join channel menuitems
	String invite_channel_menu ="img/Channel_section/Joined_Channel_Actions/menu_joinchannel_item_invite.png";
	String invite_channel_abc3 ="img/Channel_section/Joined_Channel_Actions/join_channel_invite_abc3.png";
	String invite_channel_sikuli01 ="img/Channel_section/Joined_Channel_Actions/join_channel_invite_sikuli01.png";
	String invite_channel_sikuli02 ="img/Channel_section/Joined_Channel_Actions/join_channel_invite_sikuli02.png";
	String exit_channel =path_Joined_chnannel+"menu_joinchannel_item_exitchannel.png";
	
	
	//Buttons
	// Call Audio settings
	Pattern audio_settings_channel= new Pattern("img/Channel_section/Joined_Channel_Actions/Buttons/Channel_Audio_settings.png").similar(0.9F);
	// Close Channel
	Pattern close_channel= new Pattern("img/Channel_section/Joined_Channel_Actions/Buttons/close_channel.png").similar(0.9F);
	
	
	// Call Join Menu 
	public void call_menu_joined_channel(Pattern channel_name) throws SikuliException;
	// Menu Items
	public void invite_to_channel(Pattern channel_name) throws SikuliException;
	public void find_users_channel(Pattern channel_name, Pattern menuitem_share_option) throws SikuliException;
	public void add_favorite_channel(Pattern channel_name) throws SikuliException;
	public void mute_everyone(Pattern channel_name)throws SikuliException;
	public void unmute_everyone(Pattern channel_name)throws SikuliException;
	public void speak_only(Pattern channel_name)throws SikuliException;
	public void exit_channel(Pattern channel_name)throws SikuliException;
	public void view_info(Pattern channel_name)throws SikuliException;
	
	// Buttons
	
	public void mute_unmute_button() throws SikuliException;
	public void audio_settings_button() throws SikuliException;
	public void close_button() throws SikuliException;
	
	//Send message
	public void send_message(String text) throws SikuliException;
	void channel_selection(Pattern channel_) throws SikuliException;
	void send_long_message() throws SikuliException;

	

}
