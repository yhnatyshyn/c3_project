package com.mercer.road;
import java.util.EmptyStackException;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class MainScreen implements General_temp {
	// Path to Framework images:
	String images_path = "src/main/resources/img/MainScreen/";
	String test_images_path = "src/test/resources/MainScreen/";
	// Create Sikuli screen
	Screen s = new Screen();

	// Create new pattern
	Pattern image = new Pattern();

	// Images path
	String main_menu = images_path + "main_menu.png";
	String main_menu_logout = images_path + "main_menu_logout.png";
	String main_menu_exit = images_path + "main_menu_exit.png";
	String main_menu_online_status = images_path
			+ "main_menu_online_status.png";
	String main_menu_away_status = images_path + "main_menu_away_status.png";
	String main_menu_offline_status = images_path
			+ "main_menu_offline_status.png";
	String main_menu_custom_status = images_path
			+ "main_menu_custom_status.png";
	String main_menu_custom_online_status = images_path
			+ "main_menu_custom_onine_status.png";
	String main_menu_custom_away_status = images_path
			+ "main_menu_custom_away_status.png";
	String main_menu_custom_status_save = images_path
			+ "main_menu_custom_status_save.png";
	String main_menu_custom_status_defaults = images_path
			+ "main_menu_custom_status_defaults.png";
	String main_menu_edit_profile = images_path + "main_menu_edit_profile.png";
	String edit_status_message_header = images_path
			+ "edit_status_message_header.png";
	String close_button = images_path + "close_button.png";
	String restore_window = images_path + "restore_window.png";
	String default_avatar = images_path + "default_avatar.png";
	String pictures = images_path + "pictures.png";
	String pictures_folder = images_path + "pictures_folder.png";
	String avatar = images_path + "avatar.png";
	//Expand/collapse panes
	String picture_confirm = images_path + "picture_confirm.png";
	String channels_favorites = images_path + "channels_favorites.png";
	String friends = images_path + "friends.png";
	String my_channels = images_path + "my_channels.png";
	String notifications = images_path + "notifications.png";
	//Context menu clicking
	String nitifications_icon = images_path + "nitifications_icon.png";
	String friends_icon = images_path + "friends_icon.png";
	String channels_favorites_icon = images_path + "channels_favorites_icon.png";
	String my_channels_icon = images_path + "my_channels_icon.png";
	String menu_icon = images_path + "menu_icon.png";
	
	// TestImages path
	String avatar_check = test_images_path + "avatar.png";
	String online_status = test_images_path + "online_status.png";
	String away_status = test_images_path + "away_status.png";
	String offline_status = test_images_path + "offline_status.png";
	String custom_status = test_images_path + "custom_status.png";

	Boolean launch;

	// Method performs logout via main menu from main screen.
	public void logout() throws FindFailed {
		s.wait(main_menu, 30);
		s.click(main_menu);
		s.click(main_menu_logout);
		s.wait(2.0);

	}

	// Method performs exit via main menu from main screen.
	public void exit() throws FindFailed {
		s.wait(main_menu, 30);
		s.click(main_menu);
		s.click(main_menu_exit);
	}

	// Method change user status accepts status type: online, away, offline.
	// in case of "custom" will set custom status name.
	// in case "reset" will reset to default values.
	public void set_status(String type) throws FindFailed {
		switch (type) {
		case "online": {
			s.wait(main_menu, 30);
			s.click(main_menu);
			s.click(main_menu_online_status);
			break;
		}
		case "away": {
			s.wait(main_menu, 30);
			s.click(main_menu);
			s.click(main_menu_away_status);
			break;
		}
		case "offline": {
			s.wait(main_menu, 30);
			s.click(main_menu);
			s.click(main_menu_offline_status);
			break;
		}
		case "custom": {
			s.wait(main_menu, 30);
			s.click(main_menu);
			s.click(main_menu_custom_status);
			s.click(main_menu_custom_online_status);
			s.type("a", Key.CTRL);
			s.type(Key.DELETE);
			s.type("online custom");
			s.click(main_menu_custom_away_status);
			s.type("a", Key.CTRL);
			s.type(Key.DELETE);
			s.type("away custom");
			s.click(main_menu_custom_status_save);
			break;
		}
		case "reset": {
			s.wait(main_menu, 30);
			s.click(main_menu);
			s.click(main_menu_custom_status);
			s.click(main_menu_custom_status_defaults);
			s.click(main_menu_custom_status_save);
			break;
		}
		}

	}

	// This method will check needed image on screen e.g.
	// green dot for online, yellow for away and gray for offline.
	// status - is online status for log entry.
	public void set_status_check(String status) throws FindFailed {
		switch (status) {
		case "online": {
			image.setFilename(online_status);
			try {
				s.find(image.targetOffset(0, 0).similar(0.99f));
				System.out
						.println("=========== Status changed successfully to: "
								+ status);
			} catch (FindFailed e) {
				System.out.println("=========== Status changed FAILED to: "
						+ status);
				throw new EmptyStackException();
			}
			break;
		}
		case "away": {
			image.setFilename(away_status);
			try {
				s.find(image.targetOffset(0, 0).similar(0.99f));
				System.out
						.println("=========== Status changed successfully to: "
								+ status);
			} catch (FindFailed e) {
				System.out.println("=========== Status changed FAILED to: "
						+ status);
				throw new EmptyStackException();
			}
			break;
		}

		case "offline": {
			image.setFilename(offline_status);
			try {
				s.find(image.targetOffset(0, 0).similar(0.99f));
				System.out
						.println("=========== Status changed successfully to: "
								+ status);
			} catch (FindFailed e) {
				System.out.println("=========== Status changed FAILED to: "
						+ status);
				throw new EmptyStackException();
			}
			break;
		}
		case "custom": {
			image.setFilename(custom_status);
			try {
				s.click(main_menu);
				s.click(main_menu_custom_status);
				s.find(image.targetOffset(0, 0).similar(0.95f));
				System.out
						.println("=========== Status changed successfully to: "
								+ status);
				image.setFilename(edit_status_message_header);
				s.click(image.targetOffset(160, 0).similar(0.9f));
			} catch (FindFailed e) {
				System.out.println("=========== Status changed FAILED to: "
						+ status);
				image.setFilename(edit_status_message_header);
				s.click(image.targetOffset(160, 0).similar(0.9f));
				throw new EmptyStackException();
			}
			break;
		}
		}

	}

	// Opens Edit profile dialog.
	public void edit_profile() throws FindFailed {
		s.wait(main_menu, 30);
		s.click(main_menu);
		s.click(main_menu_edit_profile);

	}

	// This class will minimize main screen
	public void minimize_window() throws FindFailed {
		image.setFilename(close_button);
		s.wait(image, 30);
		s.click(image.targetOffset(0, 0).similar(0.95f));

	}

	// This class will restore minimized main screen
	public void restore_minimized_window() throws FindFailed {
		image.setFilename(restore_window);
		s.wait(image, 30);
		s.click(image.targetOffset(0, 0).similar(0.95f));

	}

	// This class will set avatar from main screen
	public void set_avatar() throws FindFailed {
		image.setFilename(default_avatar);
		s.wait(image, 30);
		s.click(image.targetOffset(0, 0).similar(0.95f));
		image.setFilename(pictures);
		s.click(image.targetOffset(0, 0).similar(0.9f));
		image.setFilename(pictures_folder);
		s.doubleClick(image.targetOffset(0, 30).similar(0.9f));
		image.setFilename(avatar);
		s.click(image.targetOffset(0, 0).similar(0.9f));
		image.setFilename(picture_confirm);
		s.click(image.targetOffset(0, 0).similar(0.9f));
	}

	// This class will check if avatar was changed
	public void avatar_check() throws FindFailed {
		image.setFilename(avatar_check);
		s.wait(image.targetOffset(0, 0).similar(0.7f));
		System.out.println("======== Avatar was chaged!");

	}

	// This class will expand/collapse notifications section
	public void notifications_pane_click() throws FindFailed {
		image.setFilename(notifications);
		s.wait(image.similar(0.7f), 10);
		s.click(image.similar(0.7f));

	}

	// This class will expand/collapse friends section
	public void friends_pane_click() throws FindFailed {
		image.setFilename(friends);
		s.wait(image.similar(0.7f), 10);
		s.click(image.similar(0.7f));
	}

	// This class will expand/collapse channels favorites section
	public void channels_favorites_pane_click() throws FindFailed {
		image.setFilename(channels_favorites);
		s.wait(image.similar(0.7f), 10);
		s.click(image.similar(0.7f));
	}

	// This class will expand/collapse my channels section
	public void my_channels_pane_click() throws FindFailed {
		image.setFilename(my_channels);
		s.wait(image.similar(0.7f), 10);
		s.click(image.similar(0.7f));

	}

}
