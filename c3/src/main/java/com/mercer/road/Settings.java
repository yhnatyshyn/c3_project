package com.mercer.road;
import java.util.EmptyStackException;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


public class Settings implements General_temp {
		
		//Path to Framework images:
		String images_path  = "C:\\Users\\otkachenko\\Desktop\\Eclipse_workspace\\SikuliC3\\img\\Framework\\Settings\\";
	
		//Create Sikuli screen
		Screen s =new Screen();
			
		//Create new pattern
		Pattern image = new Pattern();
	
		//Images
		String edit_profile = images_path+"main_menu_edit_profile.png";
		String edit_profile_custom_name = images_path+"edit_profile_custom_name.png";
		String edit_profile_description = images_path+"edit_profile_description.png";
		String edit_profile_location = images_path+"edit_profile_location.png";
		String edit_profile_new_image = images_path+"edit_profile_new_image.png";
		String edit_profile_save = images_path+"edit_profile_save.png";
		String edit_profile_tagline = images_path+"edit_profile_tagline.png";
		String edit_profile_picture_confirm = images_path+"edit_profile_picture_confirm.png";
		String edit_profile_picture_select = images_path+"edit_profile_picture_select.png";
		String edit_profile_pictures_folder = images_path+"edit_profile_pictures_folder.png";
		String edit_profile_picture_folder = images_path+"edit_profile_picture_folder.png";
		String edit_profile_remove_image = images_path+"edit_profile_remove_image.png";
		String edit_profile_header = images_path+"edit_profile_header.png";
		
		public void edit_profile_close() throws FindFailed {
			image.setFilename(edit_profile_header);
			s.click(image.targetOffset(294, 0).similar(0.9f));
			
		}
		
		//This method sets a Tagline value
		public void edit_profile_set_tagline(String tagline) throws FindFailed {
			image.setFilename(edit_profile_tagline);
			s.click(image.targetOffset(0, 30).similar(0.9f));
			s.type("a",Key.CTRL);
			s.type(Key.DELETE);
			s.type(tagline);
		}
		
		//This method sets a Description value
		public void edit_profile_set_description(String description) throws FindFailed {
			image.setFilename(edit_profile_description);
			s.click(image.targetOffset(0, 30).similar(0.9f));
			s.type("a",Key.CTRL);
			s.type(Key.DELETE);
			s.type(description);
			
		}
		
		//This method saves changes at Edit profile dialog
		public void edit_profile_save_changes() throws FindFailed {
			s.click(edit_profile_save);
		}
		
		//This method sets a Custom name value
		public void edit_profile_custom_name(String description) throws FindFailed {
			image.setFilename(edit_profile_custom_name);
			s.click(image.targetOffset(0, 30).similar(0.9f));
			s.type("a",Key.CTRL);
			s.type(Key.DELETE);
			s.type(description);
		}
		
		//This method sets a Location value
		public void edit_profile_location(String description) throws FindFailed {
			image.setFilename(edit_profile_location);
			s.click(image.targetOffset(0, 30).similar(0.9f));
			s.type("a",Key.CTRL);
			s.type(Key.DELETE);
			s.type(description);
		}
		
		//This method sets a Profile Image
		public void edit_profile_image() throws FindFailed {
			image.setFilename(edit_profile_new_image);
			s.click(image.targetOffset(0, 30).similar(0.9f));
			image.setFilename(edit_profile_picture_folder);
			s.click(image.targetOffset(0, 0).similar(0.9f));
			image.setFilename(edit_profile_pictures_folder);
			s.doubleClick(image.targetOffset(0, 30).similar(0.9f));
			image.setFilename(edit_profile_picture_select);
			s.click(image.targetOffset(0, 0).similar(0.9f));
			image.setFilename(edit_profile_picture_confirm);
			s.click(image.targetOffset(0, 0).similar(0.9f));
		}
		
		//This method sets a Profile Image
		public void edit_profile_image_remove() throws FindFailed {
			image.setFilename(edit_profile_remove_image);
			s.click(image.targetOffset(0, 0).similar(0.9f));
		}
		
		
		 //This method will check needed image on screen e.g. 
		//if tagline, description, image was set up correctly.
		//if multiple_check is True - method won't close window after It.
		//it is done for multiple checks on one screen.
		//field - field name for log entry.
		public void edit_profile_check(String path, Boolean multiple_check, String field) throws FindFailed {
			image.setFilename(path);
			if (multiple_check == true) {
					s.find(image.similar(0.95f));
					System.out.println("=========== "+field+" changed successfully!");
				} else {
				try {
					s.find(image.similar(0.95f));
					System.out.println("=========== "+field+" changed successfully!");
					//closing window
					image.setFilename(edit_profile_header);
					s.click(image.targetOffset(294, 0).similar(0.9f));
				} catch (FindFailed e) {
					System.out.println("=========== "+field+" change FAILED!");
					//closing window
					edit_profile_close();
					//throwing exception that test fails.
					throw new EmptyStackException();
				}
			}
			
			
		}
}
