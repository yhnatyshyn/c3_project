package com.mercer.road;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.annotations.Test;



public class NewTest {
	//Path to Tests images:
	String test_images_path  = "src/main/resources/img/LongRunningTests/Test1/";
	
	
	//Creating Sikuli screen
	Screen screen = new Screen();
	Pattern image = new Pattern();
	
	MainScreen mainScreen = new MainScreen();
	Settings settings = new Settings();
	
	public int i = 1;

  @Test
  public void f() throws FindFailed, InterruptedException {
	  //Starting chat
	  image.setFilename(test_images_path+"start_chat.png");
	  screen.click(image.targetOffset(0, 0).similar(0.9f));
	  
	  //Joining channel
	  image.setFilename(test_images_path+"join_channel.png");
	  screen.click(image.targetOffset(0, 0).similar(0.9f));
	  
	  //Infinite loop
	  while (true) {
		  //Sending message to private chat
		  image.setFilename(test_images_path+"enter_chat_message.png");
		  screen.click(image.targetOffset(0, 0).similar(0.9f));
		  screen.type("Private chat message "+String.valueOf(i)+" from user1");
		  screen.type(Key.ENTER);
		  
		  //Sending message to the channel
		  image.setFilename(test_images_path+"enter_channel_message.png");
		  screen.click(image.targetOffset(0, 0).similar(0.9f));
		  screen.type("Channel message "+String.valueOf(i)+" from user1");
		  screen.type(Key.ENTER);
		  
		  //Opening settings and clicking there
		  mainScreen.edit_profile();
		  settings.edit_profile_location("New location"+String.valueOf(i));
		  settings.edit_profile_save_changes();
		  
		  //Expanding and collapsing panes
		  mainScreen.notifications_pane_click();
		  mainScreen.notifications_pane_click();
		  mainScreen.friends_pane_click();
		  mainScreen.friends_pane_click();
		  mainScreen.channels_favorites_pane_click();
		  mainScreen.channels_favorites_pane_click();
		  mainScreen.my_channels_pane_click();
		  mainScreen.my_channels_pane_click();
		  
		  //Sleeping for 60 seconds and incrementing messages counter
		  Thread.sleep(4000);
		  i++;
	  }
  }
}
